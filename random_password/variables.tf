variable "create_random_password" {
  type    = bool
  default = false
}

variable "password_length" {
  type    = number
  default = null
}
variable "special" {
  type    = bool
  default = false
}

